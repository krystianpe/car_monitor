package pl.krystianplonski.virtualcarkeys.utils;

import android.content.SharedPreferences;
import android.support.annotation.NonNull;

import com.google.firebase.iid.FirebaseInstanceId;

import java.nio.charset.Charset;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import pl.krystianplonski.virtualcarkeys.VirtualCarKeysApplication;
import timber.log.Timber;

import static android.content.Context.MODE_PRIVATE;

public class BeaconTools {

    private static final String BEACON_ID = "beaconId";
    private static final String INVALID_ID = "invalid id";
    private static final char[] hexArray = "0123456789ABCDEF".toCharArray();
    private static final String VIRTUAL_KEY = "virtual_key";
    private static final String FALLBACK_DIGEST = "0123456789abcdef0123456789abcdef";
    private final SharedPreferences sharedPrefs;

    public BeaconTools() {
        sharedPrefs = VirtualCarKeysApplication.getInstance().getSharedPreferences(VIRTUAL_KEY, MODE_PRIVATE);
    }

    private static String bytesToHex(byte[] bytes) {
        char[] hexChars = new char[bytes.length * 2];
        for (int j = 0; j < bytes.length; j++) {
            int v = bytes[j] & 0xFF;
            hexChars[j * 2] = hexArray[v >>> 4];
            hexChars[j * 2 + 1] = hexArray[v & 0x0F];
        }
        return new String(hexChars);
    }

    public String setBeaconId() {
        String beaconId = getBeaconId();
        if (beaconId.length() != 36 || beaconId.equals(INVALID_ID)) {
            String firebaseId = FirebaseInstanceId.getInstance().getId() + FirebaseInstanceId.getInstance().getToken();
            String result = createDigest(firebaseId);
            beaconId = convertDigestToBeaconId(result);
            storeBeaconId(beaconId);
        }
        return beaconId;
    }

    private void storeBeaconId(String beaconId) {
        sharedPrefs.edit().putString(BEACON_ID, beaconId).apply();
    }

    @NonNull
    private String convertDigestToBeaconId(String result) {
        StringBuilder sb = new StringBuilder(40);
        return sb.append(result.subSequence(0, 8))
                .append("-")
                .append(result.subSequence(9, 13))
                .append("-")
                .append(result.subSequence(14, 18))
                .append("-")
                .append(result.subSequence(19, 23))
                .append("-")
                .append(result.subSequence(24, 32))
                .append(result.subSequence(0, 4))
                .toString().toLowerCase();
    }

    @NonNull
    private String createDigest(String instanceId) {
        try {
            MessageDigest messageDigest = MessageDigest.getInstance("MD5");
            messageDigest.reset();
            messageDigest.update(instanceId.getBytes(Charset.forName("UTF8")));
            byte[] resultByte = messageDigest.digest();
            return bytesToHex(resultByte);
        } catch (NoSuchAlgorithmException e) {
            Timber.e(e, "md5 generation error");
        }
        return FALLBACK_DIGEST;
    }

    public String getBeaconId() {
        return sharedPrefs.getString(BEACON_ID, INVALID_ID);
    }


    public boolean isBeaconIdSet() {
        return sharedPrefs.contains(BEACON_ID);
    }


}
