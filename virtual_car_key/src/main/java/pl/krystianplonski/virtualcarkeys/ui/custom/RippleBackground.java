package pl.krystianplonski.virtualcarkeys.ui.custom;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RectF;
import android.os.Build;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.View;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.widget.RelativeLayout;


import java.util.ArrayList;
import pl.krystianplonski.virtualcarkeys.R;

public class RippleBackground extends RelativeLayout {

    private static final int DEFAULT_RIPPLE_COUNT = 6;
    private static final int DEFAULT_DURATION_TIME = 3000;
    private static final float DEFAULT_SCALE = 6.0f;
    private static final int DEFAULT_FILL_TYPE = 0;

    private Paint ripplePaint;

    private float rippleStrokeWidth;

    private boolean animationRunning = false;
    private AnimatorSet animatorSet;
    private ArrayList<RippleView> rippleViewList = new ArrayList<>();

    public RippleBackground(Context context) {
        super(context);
    }

    public RippleBackground(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }

    public RippleBackground(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context, attrs);
    }

    private void init(final Context context, final AttributeSet attrs) {
      if (isInEditMode()) {
        return;
      }

        if (null == attrs) {
            throw new IllegalArgumentException("Attributes should be provided to this view,");
        }

        final TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.RippleBackground);
        int rippleColor = typedArray.getColor(R.styleable.RippleBackground_rb_color, getResources().getColor(R.color.colorPrimary));
        rippleStrokeWidth = typedArray.getDimension(R.styleable.RippleBackground_rb_strokeWidth, getResources().getDimension(R.dimen.rippleStrokeWidth));
        float rippleRadius = typedArray.getDimension(R.styleable.RippleBackground_rb_radius, getResources().getDimension(R.dimen.rippleRadius));
        int rippleDurationTime = typedArray.getInt(R.styleable.RippleBackground_rb_duration, DEFAULT_DURATION_TIME);
        int rippleAmount = typedArray.getInt(R.styleable.RippleBackground_rb_rippleAmount, DEFAULT_RIPPLE_COUNT);
        float rippleScale = typedArray.getFloat(R.styleable.RippleBackground_rb_scale, DEFAULT_SCALE);
        int rippleType = typedArray.getInt(R.styleable.RippleBackground_rb_type, DEFAULT_FILL_TYPE);
        typedArray.recycle();

        int rippleDelay = rippleDurationTime / rippleAmount;

        ripplePaint = new Paint();
        ripplePaint.setAntiAlias(true);
        if (rippleType == DEFAULT_FILL_TYPE) {
            rippleStrokeWidth = 0;
            ripplePaint.setStyle(Paint.Style.FILL);
        } else {
            ripplePaint.setStyle(Paint.Style.STROKE);
            ripplePaint.setStrokeWidth(rippleStrokeWidth);
        }
        ripplePaint.setColor(rippleColor);

        LayoutParams rippleParams = new LayoutParams((int) (2 * (rippleRadius + rippleStrokeWidth)), (int) (2 * (rippleRadius + rippleStrokeWidth)));
        rippleParams.addRule(CENTER_IN_PARENT, TRUE);

        animatorSet = new AnimatorSet();
        animatorSet.setDuration(rippleDurationTime);
        animatorSet.setInterpolator(new AccelerateDecelerateInterpolator());
        ArrayList<Animator> animatorList = new ArrayList<>();

        for (int i = 0; i < rippleAmount; i++) {
            RippleView rippleView = new RippleView(getContext());
            addView(rippleView, rippleParams);
            rippleViewList.add(rippleView);
            final ObjectAnimator scaleXAnimator = ObjectAnimator.ofFloat(rippleView, "ScaleX", 1.0f, rippleScale);
            scaleXAnimator.setRepeatCount(ObjectAnimator.INFINITE);
            scaleXAnimator.setRepeatMode(ObjectAnimator.RESTART);
            scaleXAnimator.setStartDelay(i * rippleDelay);
            animatorList.add(scaleXAnimator);
            final ObjectAnimator scaleYAnimator = ObjectAnimator.ofFloat(rippleView, "ScaleY", 1.0f, rippleScale);
            scaleYAnimator.setRepeatCount(ObjectAnimator.INFINITE);
            scaleYAnimator.setRepeatMode(ObjectAnimator.RESTART);
            scaleYAnimator.setStartDelay(i * rippleDelay);
            animatorList.add(scaleYAnimator);
            final ObjectAnimator alphaAnimator = ObjectAnimator.ofFloat(rippleView, "Alpha", 1.0f, 0f);
            alphaAnimator.setRepeatCount(ObjectAnimator.INFINITE);
            alphaAnimator.setRepeatMode(ObjectAnimator.RESTART);
            alphaAnimator.setStartDelay(i * rippleDelay);
            animatorList.add(alphaAnimator);
        }

        animatorSet.playTogether(animatorList);
    }

    private class RippleView extends View {

        private final RectF mCircleBounds = new RectF();

        private float mRadius;

        private int mHorizontalInset = 0;
        private int mVerticalInset = 0;

        private float mTranslationOffsetX;
        private float mTranslationOffsetY;


        public RippleView(Context context) {
            super(context);
            this.setVisibility(View.INVISIBLE);
        }

        @Override
        protected void onDraw(Canvas canvas) {
            canvas.translate(mTranslationOffsetX, mTranslationOffsetY);
            canvas.drawCircle(mCircleBounds.centerX(), mCircleBounds.centerY(), mRadius, ripplePaint);
        }

        @Override
        protected void onMeasure(final int widthMeasureSpec, final int heightMeasureSpec) {
            final int height = getDefaultSize(
                    getSuggestedMinimumHeight() + getPaddingTop() + getPaddingBottom(),
                    heightMeasureSpec);
            final int width = getDefaultSize(
                    getSuggestedMinimumWidth() + getPaddingLeft() + getPaddingRight(),
                    widthMeasureSpec);

            final int diameter;
            if (heightMeasureSpec == MeasureSpec.UNSPECIFIED) {
                // ScrollView
                diameter = width;
                computeInsets(0, 0);
            } else if (widthMeasureSpec == MeasureSpec.UNSPECIFIED) {
                // HorizontalScrollView
                diameter = height;
                computeInsets(0, 0);
            } else {
                // Default
                diameter = Math.min(width, height);
                computeInsets(width - diameter, height - diameter);
            }

            setMeasuredDimension(diameter, diameter);

            final float halfWidth = diameter * 0.5f;

            final float drawnWith;
            drawnWith = rippleStrokeWidth / 2f;

            mRadius = halfWidth - drawnWith - 0.5f;

            mCircleBounds.set(-mRadius, -mRadius, mRadius, mRadius);

            mTranslationOffsetX = halfWidth + mHorizontalInset;
            mTranslationOffsetY = halfWidth + mVerticalInset;
        }

        @SuppressLint("NewApi")
        private void computeInsets(final int dx, final int dy) {
            int absoluteGravity = Gravity.CENTER;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                absoluteGravity = Gravity.getAbsoluteGravity(Gravity.CENTER, getLayoutDirection());
            }

            switch (absoluteGravity & Gravity.HORIZONTAL_GRAVITY_MASK) {
                case Gravity.LEFT:
                    mHorizontalInset = 0;
                    break;
                case Gravity.RIGHT:
                    mHorizontalInset = dx;
                    break;
                case Gravity.CENTER_HORIZONTAL:
                default:
                    mHorizontalInset = dx / 2;
                    break;
            }
            switch (absoluteGravity & Gravity.VERTICAL_GRAVITY_MASK) {
                case Gravity.TOP:
                    mVerticalInset = 0;
                    break;
                case Gravity.BOTTOM:
                    mVerticalInset = dy;
                    break;
                case Gravity.CENTER_VERTICAL:
                default:
                    mVerticalInset = dy / 2;
                    break;
            }
        }
    }

    public void startRippleAnimation() {
        if (!isRippleAnimationRunning()) {
            for (RippleView rippleView : rippleViewList) {
                rippleView.setVisibility(VISIBLE);
            }
            animatorSet.start();
            animationRunning = true;
        }
    }

    public void stopRippleAnimation() {
        if (isRippleAnimationRunning()) {
            animatorSet.end();
            animationRunning = false;
        }
    }

    public boolean isRippleAnimationRunning() {
        return animationRunning;
    }
}
