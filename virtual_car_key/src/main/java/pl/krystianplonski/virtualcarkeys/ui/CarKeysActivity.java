package pl.krystianplonski.virtualcarkeys.ui;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;

import com.trello.rxlifecycle.components.support.RxAppCompatActivity;

import org.altbeacon.beacon.Beacon;
import org.altbeacon.beacon.BeaconTransmitter;

import java.util.concurrent.TimeUnit;

import io.ghyeok.stickyswitch.widget.StickySwitch;
import pl.krystianplonski.virtualcarkeys.R;
import pl.krystianplonski.virtualcarkeys.VirtualCarKeysApplication;
import pl.krystianplonski.virtualcarkeys.databinding.CarKeysActivityBinding;
import pl.krystianplonski.virtualcarkeys.utils.BeaconTools;
import rx.Observable;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import timber.log.Timber;

import static android.support.v4.app.NotificationManagerCompat.IMPORTANCE_MAX;
import static android.view.View.GONE;

@SuppressWarnings("squid:MaximumInheritanceDepth")
public class CarKeysActivity
        extends RxAppCompatActivity implements StickySwitch.OnSelectedChangeListener, View.OnClickListener {

    private static final int NOTIFICATION_ID = 1;
    private CarKeysActivityBinding binding;
    private BeaconTools beaconTools;
    private Subscription timerSubscription;
    private boolean isPaused;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.car_keys_activity);
        if (getIntent().getExtras() != null) {
            for (String key : getIntent().getExtras().keySet()) {
                Object value = getIntent().getExtras().get(key);
                Timber.d("Key: %s Value: %s", key, value);
            }
        }
        binding.fab.setOnClickListener(this);
        binding.stickySwitch.setOnSelectedChangeListener(this);
        beaconTools = new BeaconTools();
        VirtualCarKeysApplication.getInstance().subscribeToMessages();
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (!beaconTools.isBeaconIdSet()) {
            showBeaconIdCard();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        BeaconTransmitter beaconTransmitter =
                VirtualCarKeysApplication.getInstance().getBeaconTransmission();
        binding.status.setText(beaconTransmitter.isStarted() ? R.string.beacon_transmission_started
                : R.string.beacon_transmission_stopped);
        isPaused = false;
    }

    @Override
    protected void onPause() {
        Timber.d("onPause() called ");
        super.onPause();
        isPaused = true;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        VirtualCarKeysApplication.getInstance().unsubscribeFromMessages();
        BeaconTransmitter beaconTransmitter =
                VirtualCarKeysApplication.getInstance().getBeaconTransmission();
        if (!beaconTransmitter.isStarted()) {
            beaconTransmitter.stopAdvertising();
        }
        beaconTools = null;
        if (timerSubscription != null && !timerSubscription.isUnsubscribed()) {
            timerSubscription.unsubscribe();
        }
    }


    @Override
    public void onSelectedChange(StickySwitch.Direction direction, String s) {
        Timber.d("onSelectedChange() called  with: direction = [%s], s = [%s]", direction, s);
        if (direction == StickySwitch.Direction.RIGHT) {
            switchBeaconTransmission();
            binding.responseMessage.setText(R.string.msg_subscribed);
            binding.status.setText(R.string.enabled);
            binding.dots.showNow();
            binding.rippleBackground.startRippleAnimation();
        } else {
            switchBeaconTransmission();
            binding.status.setText(R.string.disabled);
            binding.responseMessage.setText(R.string.inactive);
            binding.dots.hide();
            binding.rippleBackground.stopRippleAnimation();
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.fab:
                showBeaconIdCard();
                break;
            case R.id.ok_button:
                beaconIdCardVisibility(GONE);
                break;
            default:
        }
    }

    private void beaconIdCardVisibility(int visibility) {
        binding.beaconIdCard.setVisibility(visibility);
        binding.fab.setVisibility(visibility == View.VISIBLE ? View.GONE : View.VISIBLE);
    }


    void switchBeaconTransmission() {
        Beacon beacon = VirtualCarKeysApplication.getInstance().getBeacon(beaconTools.getBeaconId());
        BeaconTransmitter beaconTransmitter =
                VirtualCarKeysApplication.getInstance().getBeaconTransmission();
        if (beaconTransmitter.isStarted()) {
            disableKey(beaconTransmitter);
        } else {
            enableKey(beacon, beaconTransmitter);
        }
    }

    private void enableKey(Beacon beacon, BeaconTransmitter beaconTransmitter) {
        startTimeoutTimer();
        beaconTransmitter.startAdvertising(beacon);
        binding.status.setText(R.string.beacon_transmission_started);
    }

    private void disableKey(BeaconTransmitter beaconTransmitter) {
        beaconTransmitter.stopAdvertising();
        stopTimeoutTimer();
        binding.status.setText(R.string.beacon_transmission_stopped);
    }

    private void stopTimeoutTimer() {
        if (timerSubscription != null && timerSubscription.isUnsubscribed()) {
            timerSubscription.unsubscribe();
        }
    }

    private void startTimeoutTimer() {
        timerSubscription = Observable.timer(1, TimeUnit.MINUTES)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::onTimeout, this::onError, this::onCompleted);
    }

    private void onCompleted() {
        Timber.d("timer life has ended");
    }

    private void onError(Throwable throwable) {
        Timber.e(throwable, "error occurred in timer");
    }

    private void onTimeout(long timeout) {
        Timber.d("onTimeout() called  with: timeout = [%s]", timeout);
        showNotification();
        binding.status.setText(R.string.timeout);
        binding.stickySwitch.setDirection(StickySwitch.Direction.LEFT);
    }

    private void showNotification() {
        if (isPaused) {
            Intent intent = new Intent(this, CarKeysActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent,
                    PendingIntent.FLAG_ONE_SHOT);

            Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            android.support.v4.app.NotificationCompat.Builder notificationBuilder = new android.support.v4.app.NotificationCompat.Builder(this)
                    .setSmallIcon(R.drawable.ic_lock_open)
                    .setContentTitle("Virtual Key")
                    .setContentText("No response from server!")
                    .setPriority(IMPORTANCE_MAX)
                    .setAutoCancel(true)
                    .setSound(defaultSoundUri)
                    .setContentIntent(pendingIntent);

            NotificationManager notificationManager =
                    (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

            notificationManager.notify(1, notificationBuilder.build());
        }
    }

    private void showBeaconIdCard() {
        binding.beaconId.setText(beaconTools.setBeaconId());
        beaconIdCardVisibility(View.VISIBLE);
        binding.okButton.setOnClickListener(this);
    }

}

