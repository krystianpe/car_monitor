package pl.krystianplonski.virtualcarkeys;

import android.app.Application;
import android.content.Context;
import android.support.multidex.MultiDex;
import android.util.Log;

import com.facebook.stetho.Stetho;
import com.google.firebase.messaging.FirebaseMessaging;
import com.jakewharton.threetenabp.AndroidThreeTen;

import org.altbeacon.beacon.AltBeacon;
import org.altbeacon.beacon.Beacon;
import org.altbeacon.beacon.BeaconParser;
import org.altbeacon.beacon.BeaconTransmitter;
import org.altbeacon.beacon.powersave.BackgroundPowerSaver;

import java.util.Collections;

import javax.annotation.Nonnull;

import timber.log.Timber;

public final class VirtualCarKeysApplication extends Application {

    private static final String TAG = "VirtualCarKeysApplicati";
    private static VirtualCarKeysApplication instance;
    private BackgroundPowerSaver backgroundPowerSaver;
    private BeaconTransmitter beaconTransmitter;
    private Beacon beacon;
    private boolean subscribed;

    public VirtualCarKeysApplication() {
        instance = this;
    }

    public static VirtualCarKeysApplication getInstance() {
        return instance;
    }

    public Beacon getBeacon(@Nonnull String id) {
        if (beacon == null) {
            beacon = new AltBeacon.Builder()
                    .setId1(id)
                    .setId2("1")
                    .setId3("2")
                    .setManufacturer(0x0118)
                    .setTxPower(-59)
                    .setDataFields(Collections.singletonList(0L))
                    .build();
        }
        return beacon;
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        AndroidThreeTen.init(this);
        if (BuildConfig.DEBUG) {
            Timber.plant(new Timber.DebugTree());
            Stetho.initializeWithDefaults(this);
        } else {
            Timber.plant(new CrashReportingTree());
        }
        backgroundPowerSaver = new BackgroundPowerSaver(this);
    }

    @Override
    public void onTerminate() {
        super.onTerminate();
        unsubscribeFromMessages();
    }

    public BeaconTransmitter getBeaconTransmission() {
        if (beaconTransmitter == null) {
            BeaconParser beaconParser = new BeaconParser()
                    .setBeaconLayout("m:2-3=beac,i:4-19,i:20-21,i:22-23,p:24-24,d:25-25");
            beaconTransmitter = new BeaconTransmitter(this, beaconParser);
        }
        return beaconTransmitter;
    }

    public void subscribeToMessages() {
        if (!subscribed) {
            subscribed = true;
            FirebaseMessaging.getInstance().subscribeToTopic("car_status");
        }
    }

    public void unsubscribeFromMessages() {
        if (subscribed) {
            subscribed = false;
            FirebaseMessaging.getInstance().unsubscribeFromTopic("car_status");
        }
    }

    /**
     * A tree which logs important information for crash reporting.
     */
    private static class CrashReportingTree extends Timber.Tree {
        @Override
        protected void log(int priority, String tag, String message, Throwable t) {
            if (priority == Log.VERBOSE || priority == Log.DEBUG) {
                return;
            }

            /*FakeCrashLibrary.log(priority, tag, message);

            if (t != null) {
                if (priority == Log.ERROR) {
                    FakeCrashLibrary.logError(t);
                } else if (priority == Log.WARN) {
                    FakeCrashLibrary.logWarning(t);
                }
            }*/
        }
    }
}
