package pl.krystianplonski.carmonitor.receiver;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.PowerManager;

import java.util.Calendar;

import pl.krystianplonski.carmonitor.ui.alarm.AlarmReceiverActivity;

public class HrmAlarmReceiver extends BroadcastReceiver {

    @Override public void onReceive(Context context, Intent intent) {
    PowerManager pm = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
    PowerManager.WakeLock wl = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, "");
    wl.acquire();
  }

  public void setAlarm(Context context) {

    Calendar cal = Calendar.getInstance();
    Intent intent = new Intent(context, AlarmReceiverActivity.class);
    PendingIntent pendingIntent =
        PendingIntent.getActivity(context, 12345, intent, PendingIntent.FLAG_CANCEL_CURRENT);
    AlarmManager am = (AlarmManager) context.getSystemService(Activity.ALARM_SERVICE);
    am.set(AlarmManager.RTC_WAKEUP, cal.getTimeInMillis(), pendingIntent);
  }
}