package pl.krystianplonski.carmonitor.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;

import pl.krystianplonski.carmonitor.CarMonitorApplication;
import pl.krystianplonski.carmonitor.ui.MainActivity;
import timber.log.Timber;

import static pl.krystianplonski.carmonitor.utils.Constants.LAST_TIME;

public class PlugInControlReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        Timber.d("onReceive() called  with: context = [%s], intent = [%s]", context, intent);
        String action = intent.getAction();

        if (action.equals(Intent.ACTION_POWER_CONNECTED)) {
            startListeningForKey(context);
        } else if (action.equals(Intent.ACTION_POWER_DISCONNECTED)) {
            clearLastKeyRegistrationTime();
        }
    }

    private void clearLastKeyRegistrationTime() {
        SharedPreferences sharedPreferences = CarMonitorApplication.getInstance().getSharedPreferences("keys", Context.MODE_PRIVATE);
        sharedPreferences.edit().putLong(LAST_TIME, 0).apply();
    }

    private void startListeningForKey(Context context) {
        Intent intent = new Intent(context, MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(intent);
    }
}