package pl.krystianplonski.carmonitor.model;

import com.squareup.moshi.Json;

public class Message {
    @Json(name = "to")
    String to;
    @Json(name = "data")
    NotifyData notification;

    public Message(String to, NotifyData notification) {
        this.to = to;
        this.notification = notification;
    }

}