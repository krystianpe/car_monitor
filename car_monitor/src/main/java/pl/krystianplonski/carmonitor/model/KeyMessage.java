package pl.krystianplonski.carmonitor.model;

public class KeyMessage {

    private String mName;
    private String mMessage;
    private String mUid;

    public KeyMessage() {
        // Needed for Firebase
    }

    public KeyMessage(String name, String message, String uid) {
        mName = name;
        mMessage = message;
        mUid = uid;
    }

    public String getName() {
        return mName;
    }

    public void setName(String name) {
        mName = name;
    }

    public String getMessage() {
        return mMessage;
    }

    public void setMessage(String message) {
        mMessage = message;
    }

    public String getUid() {
        return mUid;
    }

    public void setUid(String uid) {
        mUid = uid;
    }
}