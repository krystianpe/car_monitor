package pl.krystianplonski.carmonitor.model;

public class NotifyData {
    String title;
    String body;

    public NotifyData(String title, String body) {
        this.title = title;
        this.body = body;
    }

}