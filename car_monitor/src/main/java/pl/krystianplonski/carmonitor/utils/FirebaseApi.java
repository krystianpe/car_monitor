package pl.krystianplonski.carmonitor.utils;

import pl.krystianplonski.carmonitor.model.Message;
import retrofit2.http.Body;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import rx.Observable;

public interface FirebaseApi {
    @Headers({
            "Authorization:key=AIzaSyB_Mlz8YrVFk_Mx8ReMKlQLvh1ORxbwVtk"
    })
    @POST("/fcm/send")
    Observable<Message> sendMessage(@Body Message message);

}