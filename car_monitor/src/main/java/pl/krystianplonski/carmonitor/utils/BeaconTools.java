package pl.krystianplonski.carmonitor.utils;

import android.content.SharedPreferences;

import org.altbeacon.beacon.Identifier;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import pl.krystianplonski.carmonitor.CarMonitorApplication;

import static android.content.Context.MODE_PRIVATE;

public class BeaconTools {

    private static final String BEACON_ID = "beaconId";
    private static final String INVALID_ID = "invalid id";
    private static final String VIRTUAL_KEY = "virtual_key";
    private final SharedPreferences sharedPrefs;
    private Pattern idPattern = Pattern.compile("^[a-f0-9]{8}-[a-f0-9]{4}-[a-f0-9]{4}-[a-f0-9]{4}-[a-f0-9]{12}");

    public BeaconTools() {
        sharedPrefs = CarMonitorApplication.getInstance().getSharedPreferences(VIRTUAL_KEY, MODE_PRIVATE);
    }

    public void storeBeaconId(String beaconId) {
        sharedPrefs.edit().putString(BEACON_ID, beaconId).apply();
    }

    public String getBeaconId() {
        return sharedPrefs.getString(BEACON_ID, INVALID_ID);
    }

    @Nullable
    public Identifier getBeaconIdentifier() {
        if (sharedPrefs.contains(BEACON_ID)) {
            return Identifier.parse(BEACON_ID);
        } else {
            return null;
        }
    }

    public boolean isBeaconIdSet() {
        return sharedPrefs.contains(BEACON_ID);
    }

    public boolean isValid(@Nonnull String id) {
        Matcher matcher = idPattern.matcher(id);
        return id.length() == 36 && matcher.find();
    }
}
