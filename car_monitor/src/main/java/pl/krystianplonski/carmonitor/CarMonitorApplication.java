package pl.krystianplonski.carmonitor;

import android.app.Application;
import android.content.Context;
import android.support.multidex.MultiDex;
import android.util.Log;

import com.facebook.stetho.Stetho;
import com.facebook.stetho.okhttp3.StethoInterceptor;
import com.jakewharton.threetenabp.AndroidThreeTen;

import org.altbeacon.beacon.BeaconManager;
import org.altbeacon.beacon.Region;
import org.altbeacon.beacon.powersave.BackgroundPowerSaver;
import org.altbeacon.beacon.startup.BootstrapNotifier;
import org.altbeacon.beacon.startup.RegionBootstrap;

import okhttp3.OkHttpClient;
import pl.krystianplonski.carmonitor.utils.BeaconTools;
import pl.krystianplonski.carmonitor.utils.FirebaseApi;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.moshi.MoshiConverterFactory;
import timber.log.Timber;

public final class CarMonitorApplication extends Application implements BootstrapNotifier {

    private static CarMonitorApplication instance;
    private RegionBootstrap regionBootstrap;
    private BackgroundPowerSaver backgroundPowerSaver;
    private BeaconManager beaconManager;
    private FirebaseApi firebaseApi;
    private BeaconTools beaconTools;

    public CarMonitorApplication() {
        instance = this;
    }

    public static CarMonitorApplication getInstance() {
        return instance;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        AndroidThreeTen.init(this);
        if (BuildConfig.DEBUG) {
            Timber.plant(new Timber.DebugTree());
            Stetho.initializeWithDefaults(this);
        } else {
            Timber.plant(new CrashReportingTree());
        }
        beaconTools = new BeaconTools();
        beaconManager = BeaconManager.getInstanceForApplication(this);
        // wake up the app when any beacon is seen (you can specify specific id filers in the parameters below)
        Region region = new Region("com.krystianplonski.carmonitor.boostrapRegion",
                beaconTools.getBeaconIdentifier(), null, null);
        regionBootstrap = new RegionBootstrap(this, region);
        backgroundPowerSaver = new BackgroundPowerSaver(this);
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }

    @Override
    public void didDetermineStateForRegion(int arg0, Region arg1) {
        // ignored
    }

    @Override
    public void didEnterRegion(Region arg0) {
        regionBootstrap.disable();
    }

    @Override
    public void didExitRegion(Region arg0) {
        // Don't care
    }

    public BeaconTools getBeaconTools() {
        return beaconTools;
    }

    public BeaconManager getBeaconManager() {
        return beaconManager;
    }

    public FirebaseApi getFirebaseApi() {
        if (firebaseApi == null) {
            OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
            httpClient.addNetworkInterceptor(new StethoInterceptor());

            OkHttpClient client = httpClient.build();

            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl("https://fcm.googleapis.com")
                    .client(client)
                    .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                    .addConverterFactory(MoshiConverterFactory.create())
                    .build();

            firebaseApi = retrofit.create(FirebaseApi.class);
        }
        return firebaseApi;
    }

    /**
     * A tree which logs important information for crash reporting.
     */
    private static class CrashReportingTree extends Timber.Tree {
        @Override
        protected void log(int priority, String tag, String message, Throwable t) {
            if (priority == Log.VERBOSE || priority == Log.DEBUG) {
                return;
            }

            /*FakeCrashLibrary.log(priority, tag, message);

            if (t != null) {
                if (priority == Log.ERROR) {
                    FakeCrashLibrary.logError(t);
                } else if (priority == Log.WARN) {
                    FakeCrashLibrary.logWarning(t);
                }
            }*/
        }
    }
}
