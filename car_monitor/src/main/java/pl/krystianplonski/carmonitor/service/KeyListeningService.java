package pl.krystianplonski.carmonitor.service;

import android.app.IntentService;
import android.content.Intent;
import android.os.RemoteException;
import android.support.annotation.Nullable;

import org.altbeacon.beacon.BeaconConsumer;
import org.altbeacon.beacon.BeaconManager;
import org.altbeacon.beacon.MonitorNotifier;
import org.altbeacon.beacon.Region;

import java.util.concurrent.TimeUnit;

import pl.krystianplonski.carmonitor.CarMonitorApplication;
import rx.Observable;
import rx.Observer;
import timber.log.Timber;

public class KeyListeningService extends IntentService implements BeaconConsumer, MonitorNotifier, Observer<Integer> {

    private BeaconManager beaconManager;

    public KeyListeningService() {
        super("KeyListeningService");
    }

    public KeyListeningService(String name) {
        super(name);
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {
        Timber.d("onHandleIntent() called  with: intent = [%s]", intent);
        beaconManager = CarMonitorApplication.getInstance().getBeaconManager();
        beaconManager.bind(this);
        startCounting();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        beaconManager.unbind(this);
    }

    private void startCounting() {
        Observable.just(1).debounce(5, TimeUnit.MINUTES).subscribe(this);
        //TODO wait 5 minutes for beacon to show up otherwise take a photo of the driver
    }

    @Override
    public void onBeaconServiceConnect() {
        beaconManager.addMonitorNotifier(this);
        try {
            beaconManager.startMonitoringBeaconsInRegion(new Region("myMonitoringUniqueId", null, null, null));
        } catch (RemoteException e) {
            Timber.e("onBeaconServiceConnect: ", e);
        }
    }

    @Override
    public void didEnterRegion(Region region) {
        Timber.d("didEnterRegion() called  with: region = [%s]", region);
       /* getActivity().runOnUiThread(() -> binding.statusMessage.setText(R.string.detected_beacon));*/
    }

    @Override
    public void didExitRegion(Region region) {
        Timber.d("didExitRegion() called  with: region = [%s]", region);
      /*  getActivity().runOnUiThread(() -> binding.statusMessage.setText(R.string.no_beacon_detected));*/
    }

    @Override
    public void didDetermineStateForRegion(int state, Region region) {
        Timber.d("didDetermineStateForRegion() state = [%s], region = [%s]", state, region);
        if (state == 1) {
            if (!beaconManager.removeMonitoreNotifier(this)) {
                Timber.e("failed removing beacon monitor");
            } else {
                Timber.d("removed notifier");
                beaconManager.unbind(this);
            }
        }
        /*getActivity().runOnUiThread(() ->
                binding.statusMessage.setText(
                        state == 1 ? R.string.car_opened : R.string.waiting_for_keys));*/
    }

    @Override
    public void onCompleted() {
        Timber.d("onCompleted() called ");
    }

    @Override
    public void onError(Throwable throwable) {
        Timber.d(throwable, "onError: [throwable]");
    }

    @Override
    public void onNext(Integer integer) {
        Timber.d("onNext %d", integer);
    }
}
