package pl.krystianplonski.carmonitor.service;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;

import pl.krystianplonski.carmonitor.receiver.HrmAlarmReceiver;

public class HrmAlarmService extends Service {
  HrmAlarmReceiver alarm = new HrmAlarmReceiver();

  @Override public int onStartCommand(Intent intent, int flags, int startId) {
    alarm.setAlarm(this);
    return START_STICKY;
  }

  @Override public IBinder onBind(Intent intent) {
    return null;
  }
}
