package pl.krystianplonski.carmonitor.ui.alarm;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;

import java.io.IOException;

import pl.krystianplonski.carmonitor.R;
import pl.krystianplonski.carmonitor.service.HrmAlarmService;
import timber.log.Timber;

public class AlarmReceiverActivity extends Activity {
  private MediaPlayer mMediaPlayer;

  @Override public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    this.requestWindowFeature(Window.FEATURE_NO_TITLE);
    this.getWindow()
        .setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
            WindowManager.LayoutParams.FLAG_FULLSCREEN);
    setContentView(R.layout.alarm_receiver_activity);

    ImageView stopAlarm = (ImageView) findViewById(R.id.stopAlarm);
    stopAlarm.setOnTouchListener((arg0, arg1) -> {
      mMediaPlayer.stop();
      stopService(new Intent(this, HrmAlarmService.class));
      finish();
      return false;
    });

    playSound(this, getAlarmUri());
  }

  @Override
  public void onBackPressed() {
    //do not allow backPress, exit only via icon click
  }

  private void playSound(Context context, Uri alert) {
    mMediaPlayer = new MediaPlayer();
    try {
      mMediaPlayer.setDataSource(context, alert);
      final AudioManager audioManager =
          (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);
      if (audioManager.getStreamVolume(AudioManager.STREAM_ALARM) != 0) {
        mMediaPlayer.setAudioStreamType(AudioManager.STREAM_ALARM);
        mMediaPlayer.prepare();
        mMediaPlayer.start();
      }
    } catch (IOException e) {
      Timber.d("error %s", e.getMessage());
    }
  }

  private Uri getAlarmUri() {
    Uri alert = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_ALARM);
    if (alert == null) {
      alert = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
      if (alert == null) {
        alert = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_RINGTONE);
      }
    }
    return alert;
  }
}