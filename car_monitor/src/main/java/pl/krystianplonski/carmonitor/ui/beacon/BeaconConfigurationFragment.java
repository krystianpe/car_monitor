package pl.krystianplonski.carmonitor.ui.beacon;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.trello.rxlifecycle.components.RxFragment;

import pl.krystianplonski.carmonitor.CarMonitorApplication;
import pl.krystianplonski.carmonitor.R;
import pl.krystianplonski.carmonitor.databinding.FragmentBeaconConfigurationBinding;
import pl.krystianplonski.carmonitor.ui.ActivityCallback;
import pl.krystianplonski.carmonitor.ui.MainActivity;
import pl.krystianplonski.carmonitor.utils.BeaconTools;

public class BeaconConfigurationFragment extends RxFragment implements View.OnClickListener {

    private FragmentBeaconConfigurationBinding binding;
    private ActivityCallback activityCallback;
    private BeaconTools beaconTools;


    public static BeaconConfigurationFragment getInstance() {
        return new BeaconConfigurationFragment();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_beacon_configuration, container, false);
        binding.okButton.setOnClickListener(this);
        beaconTools = CarMonitorApplication.getInstance().getBeaconTools();
        return binding.getRoot();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        activityCallback = (MainActivity) context;
    }

    @Override
    public void onClick(View v) {
        String id = binding.beaconId.getText().toString();
        if (beaconTools.isValid(id)) {
            beaconTools.storeBeaconId(id);
            activityCallback.startKey();
        } else {
            Toast.makeText(getActivity(), R.string.wrong_id, Toast.LENGTH_LONG).show();
        }
    }
}
