package pl.krystianplonski.carmonitor.ui;

import android.Manifest;
import android.bluetooth.BluetoothAdapter;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.design.widget.BottomNavigationView;
import android.widget.Toast;

import com.firebase.ui.auth.AuthUI;
import com.google.firebase.auth.FirebaseAuth;
import com.tbruyelle.rxpermissions.RxPermissions;
import com.trello.rxlifecycle.components.support.RxAppCompatActivity;

import pl.krystianplonski.carmonitor.CarMonitorApplication;
import pl.krystianplonski.carmonitor.R;
import pl.krystianplonski.carmonitor.databinding.MainActivityBinding;
import pl.krystianplonski.carmonitor.ui.app.AppFragment;
import pl.krystianplonski.carmonitor.ui.beacon.BeaconConfigurationFragment;
import pl.krystianplonski.carmonitor.ui.camera.FaceTrackerActivity;
import pl.krystianplonski.carmonitor.ui.hrm.HrmFragment;
import pl.krystianplonski.carmonitor.ui.keys.KeysFragment;
import pl.krystianplonski.carmonitor.utils.BeaconTools;
import timber.log.Timber;

@SuppressWarnings("squid:MaximumInheritanceDepth") public class MainActivity
        extends RxAppCompatActivity implements ActivityCallback {

    private static final String HRM = "HRM";
    private static final String KEY = "KEY";
    private static final String CAMERA = "CAMERA";
    private static final int BT_ENABLE = 111;
    private static final int SIGN_IN_REQUEST_CODE = 222;
    private MainActivityBinding binding;
    private BeaconTools beaconTools;
    private BottomNavigationView.OnNavigationItemSelectedListener onNavigationItemSelectedListener =
            item -> {
                switch (item.getItemId()) {
                    case R.id.navigation_hrm:
                        showHRM();
                        return true;
                    case R.id.camera_view:
                        startCamera();
                        return true;
                    case R.id.navigation_key:
                    default:
                        startKey();
                        return true;
                }
            };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.main_activity);
        beaconTools = CarMonitorApplication.getInstance().getBeaconTools();
        RxPermissions rxPermissions = new RxPermissions(this);
        rxPermissions.request(Manifest.permission.ACCESS_COARSE_LOCATION,
                Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.CAMERA,
                Manifest.permission.BLUETOOTH, Manifest.permission.BLUETOOTH_ADMIN, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                .subscribe(granted -> {
                    if (granted) {
                        BluetoothAdapter mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
                        if (mBluetoothAdapter == null) {
                            Toast.makeText(this.getParent(), "Bluetooth is not supported on this device", Toast.LENGTH_LONG).show();
                        } else if (!mBluetoothAdapter.isEnabled()) {
                            Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                            startActivityForResult(enableBtIntent, BT_ENABLE);
                        } else {
                            showEntryScreen();
                        }
                    } else {
                        Toast.makeText(this, "Permissions not granted", Toast.LENGTH_LONG).show();
                    }
                });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        Timber.d("onActivityResult() called  with: requestCode = [%s], resultCode = [%s], data = [%s]", requestCode, resultCode, data);
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == BT_ENABLE) {
            if (resultCode == RESULT_OK) {
                showEntryScreen();
            }
        } else if (requestCode == SIGN_IN_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                Toast.makeText(this,
                        "Successfully signed in. Welcome!",
                        Toast.LENGTH_LONG)
                        .show();
                showEntryScreen();
            } else {
                Toast.makeText(this,
                        "We couldn't sign you in. Please try again later.",
                        Toast.LENGTH_LONG)
                        .show();

                // Close the app
                finish();
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        //binding.navigation.setSelectedItemId(R.id.navigation_key);
    }

    @Override
    public void startCamera() {
        Intent cameraIntent = new Intent(this, FaceTrackerActivity.class);
        startActivity(cameraIntent);
    }

    @Override
    public void startHRM() {
        //   binding.navigation.setSelectedItemId(R.id.navigation_hrm);
    }

    @Override
    public void startKey() {
        if (beaconTools.isBeaconIdSet()) {
            showKey();
        } else {
            showBeaconConfiguration();
        }
        //    binding.navigation.setSelectedItemId(R.id.navigation_key);
    }

    private void showBeaconConfiguration() {
        getFragmentManager().beginTransaction().replace(R.id.container, BeaconConfigurationFragment.getInstance(),
                HRM).commit();
    }

    private void showHRM() {
        getFragmentManager().beginTransaction().replace(R.id.container, HrmFragment.getInstance(),
                HRM).commit();
    }

    private void showApp() {
        getFragmentManager().beginTransaction().replace(R.id.container, AppFragment.getInstance(),
                null).commit();
    }

    private void showEntryScreen() {
        authenticate();
        startKey();
        binding.navigation.setOnNavigationItemSelectedListener(onNavigationItemSelectedListener);
    }

    private void authenticate() {
        if (FirebaseAuth.getInstance().getCurrentUser() == null) {
            startActivityForResult(
                    AuthUI.getInstance()
                            .createSignInIntentBuilder()
                            .build(),
                    SIGN_IN_REQUEST_CODE
            );
        } else {
            Toast.makeText(this,
                    "Welcome " + FirebaseAuth.getInstance()
                            .getCurrentUser()
                            .getDisplayName(),
                    Toast.LENGTH_LONG)
                    .show();
        }
    }

    private void showKey() {
        getFragmentManager().beginTransaction().replace(R.id.container, KeysFragment.getInstance(),
                KEY).commit();
    }


}

