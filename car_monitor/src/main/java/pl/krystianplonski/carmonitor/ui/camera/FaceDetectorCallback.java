package pl.krystianplonski.carmonitor.ui.camera;

interface FaceDetectorCallback {
    void faceDetected(GraphicOverlay mOverlay);
}