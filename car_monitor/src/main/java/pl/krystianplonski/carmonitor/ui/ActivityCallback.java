package pl.krystianplonski.carmonitor.ui;

public interface ActivityCallback {

    void startHRM();

    void startCamera();

    void startKey();
}
