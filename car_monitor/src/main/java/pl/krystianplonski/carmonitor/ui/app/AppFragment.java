package pl.krystianplonski.carmonitor.ui.app;

import android.app.Fragment;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import pl.krystianplonski.carmonitor.R;
import pl.krystianplonski.carmonitor.databinding.FragmentAppBinding;

public class AppFragment extends Fragment {

    private FragmentAppBinding binding;

    public static AppFragment getInstance() {
        return new AppFragment();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_app, container, false);
        return binding.getRoot();
    }
}
