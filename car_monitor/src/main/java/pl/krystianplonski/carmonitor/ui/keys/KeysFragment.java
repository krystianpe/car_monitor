package pl.krystianplonski.carmonitor.ui.keys;

import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.os.RemoteException;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.iid.FirebaseInstanceId;
import com.trello.rxlifecycle.components.RxFragment;

import org.altbeacon.beacon.BeaconConsumer;
import org.altbeacon.beacon.BeaconManager;
import org.altbeacon.beacon.MonitorNotifier;
import org.altbeacon.beacon.Region;

import java.util.concurrent.TimeUnit;

import pl.krystianplonski.carmonitor.CarMonitorApplication;
import pl.krystianplonski.carmonitor.R;
import pl.krystianplonski.carmonitor.databinding.FragmentKeysBinding;
import pl.krystianplonski.carmonitor.model.KeyMessage;
import pl.krystianplonski.carmonitor.model.Message;
import pl.krystianplonski.carmonitor.model.NotifyData;
import pl.krystianplonski.carmonitor.ui.ActivityCallback;
import pl.krystianplonski.carmonitor.ui.MainActivity;
import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import timber.log.Timber;

import static pl.krystianplonski.carmonitor.utils.Constants.LAST_TIME;

public class KeysFragment extends RxFragment implements BeaconConsumer, MonitorNotifier {

    private FragmentKeysBinding binding;
    private BeaconManager beaconManager;
    private ActivityCallback activityCallback;

    public static KeysFragment getInstance() {
        return new KeysFragment();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_keys, container, false);
        return binding.getRoot();
    }

    @Override
    public void onResume() {
        super.onResume();
        startBeaconSearching();
    }

    @Override
    public void onStop() {
        super.onStop();
        disconnectBeaconManager();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        activityCallback = (MainActivity) context;
    }

    @Override
    public void onBeaconServiceConnect() {
        beaconManager.addMonitorNotifier(this);
        try {
            beaconManager.startMonitoringBeaconsInRegion(new Region("myMonitoringUniqueId", null, null, null));
        } catch (RemoteException e) {
            Timber.e("onBeaconServiceConnect: ", e);
            binding.statusMessage.setText(e.getMessage());
        }
    }

    @Override
    public void didEnterRegion(Region region) {
        Timber.d("didEnterRegion() called  with: region = [%s]", region);
        setKeyRegistrationTime();
        getActivity().runOnUiThread(() -> binding.statusMessage.setText(R.string.detected_beacon));

    }

    @Override
    public void didExitRegion(Region region) {
        Timber.d("didExitRegion() called  with: region = [%s]", region);
        getActivity().runOnUiThread(() -> binding.statusMessage.setText(R.string.no_beacon_detected));
    }

    @Override
    public void didDetermineStateForRegion(int state, Region region) {
        Timber.d("didDetermineStateForRegion() state = [%s], region = [%s]", state, region);
        getActivity().runOnUiThread(() ->
                binding.statusMessage.setText(
                        state == 1 ? R.string.car_opened : R.string.waiting_for_keys));
        sendStatus(true);
    }

    @Override
    public Context getApplicationContext() {
        return CarMonitorApplication.getInstance();
    }

    @Override
    public void unbindService(ServiceConnection serviceConnection) {
        getActivity().unbindService(serviceConnection);
    }

    @Override
    public boolean bindService(Intent intent, ServiceConnection serviceConnection, int i) {
        return getActivity().bindService(intent, serviceConnection, i);
    }

    private void setKeyRegistrationTime() {
        SharedPreferences sharedPreferences = getApplicationContext()
                .getSharedPreferences("keys", Context.MODE_PRIVATE);
        sharedPreferences.edit().putLong(LAST_TIME, System.currentTimeMillis()).apply();
    }

    private long getKeyRegistrationTime() {
        SharedPreferences sharedPreferences = getApplicationContext()
                .getSharedPreferences("keys", Context.MODE_PRIVATE);
        return sharedPreferences.getLong(LAST_TIME, 0);
    }

    private void startBeaconSearching() {
        if (!isAlreadyUnlocked()) {
            beaconManager = CarMonitorApplication.getInstance().getBeaconManager();
            beaconManager.bind(this);
            binding.statusMessage.setText(R.string.waiting_for_keys);
            startCameraTimer();
        } else {
            searchingNotNecessary();
        }
    }

    private void startCameraTimer() {
        Observable.timer(1, TimeUnit.MINUTES)
                .flatMap(ignored -> sendStatus(false))
                .observeOn(Schedulers.io())
                .compose(bindToLifecycle())
                .subscribeOn(AndroidSchedulers.mainThread())
                .subscribe(this::onResponse, this::onError, activityCallback::startCamera);
    }

    private Observable<Message> sendStatus(boolean isSuccess) {
        FirebaseDatabase.getInstance()
                .getReference()
                .push()
                .setValue(new KeyMessage("key-monitor",
                        isSuccess ? "unlock successful" : "unlock unsuccessful",
                        FirebaseInstanceId.getInstance().getToken()));
        NotifyData notifydata = new NotifyData("Key Monitor",
                isSuccess ? "unlock successful" : "unlock unsuccessful");
        return CarMonitorApplication.getInstance().getFirebaseApi()
                .sendMessage(new Message("/topics/key-status", notifydata));
    }

    private void searchingNotNecessary() {
        binding.statusMessage.setText(R.string.already_unlocked);
        Observable.timer(10, TimeUnit.SECONDS)
                .compose(bindToLifecycle())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnCompleted(activityCallback::startHRM)
                .subscribe();
    }

    private boolean isAlreadyUnlocked() {
        return getKeyRegistrationTime() > 0;
    }

    private void disconnectBeaconManager() {
        if (beaconManager != null) {
            beaconManager.unbind(this);
            if (!beaconManager.removeMonitoreNotifier(this)) {
                Timber.e("failed removing beacon monitor");
            }
        }
    }

    private void onError(Throwable throwable) {
        Timber.e(throwable, "onFailure");
    }

    public void onResponse(Message response) {
        Timber.d("Response %s", response);
    }

}
