package pl.krystianplonski.carmonitor.ui.hrm;

import android.app.Fragment;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.polidea.rxandroidble.RxBleClient;
import com.polidea.rxandroidble.RxBleDevice;

import java.util.UUID;
import java.util.concurrent.TimeUnit;

import pl.krystianplonski.carmonitor.R;
import pl.krystianplonski.carmonitor.databinding.FragmentHrmBinding;
import pl.krystianplonski.carmonitor.service.HrmAlarmService;
import rx.Observable;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import timber.log.Timber;

import static pl.krystianplonski.carmonitor.GattAttributes.HEART_RATE_MEASUREMENT;

public class HrmFragment extends Fragment {

    static final String HRM_MAC = "FB:6B:97:72:A6:29";
    private FragmentHrmBinding binding;
    private RxBleClient rxBleClient;

    private Subscription scanSubscription;
    private Subscription alarmSubscription;
    private RxBleDevice device;

    public static HrmFragment getInstance() {
        return new HrmFragment();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        rxBleClient = RxBleClient.create(getActivity());
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_hrm, container, false);
        return binding.getRoot();
    }

    @Override
    public void onResume() {
        super.onResume();
        Timber.d("onResume Hrm");
        binding.deviceStatus.setText(R.string.connecting);
        binding.pulseWarningIcon.setVisibility(View.GONE);
        binding.heartIcon.setVisibility(View.GONE);
        binding.pulsator.stop();
        binding.bleRestart.setVisibility(View.VISIBLE);
        connectToDevice();
        scanForHR();
               /* .flatMap(RxBleConnection::discoverServices)
                .flatMap(services -> Observable.interval(2, TimeUnit.SECONDS)
                        .flatMap(t -> services.getCharacteristic(UUID.fromString(HEART_RATE_MEASUREMENT))))
                .subscribe(this::onCharacteristicReceived, this::onGetCharacteristicError);*/
    }

    @Override
    public void onPause() {
        super.onPause();
        binding.statusMessage.setText(null);
        binding.pulseWarningIcon.setVisibility(View.GONE);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (isScanning()) {
            scanSubscription.unsubscribe();
        }
        if (alarmSubscription != null) {
            alarmSubscription.unsubscribe();
        }
        clearSubscription();
    }

    private void scanForHR() {
        if (scanSubscription == null) {
            scanSubscription = device.establishConnection(true)
                    .flatMap(rxBleConnection -> rxBleConnection.setupNotification(
                            UUID.fromString(HEART_RATE_MEASUREMENT)))
                    .doOnNext(
                            notificationObservable -> getActivity().runOnUiThread(this::notificationHasBeenSetUp))
                    .flatMap(observable -> observable)
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(this::onNotificationReceived, this::onNotificationSetupFailure);
        }
    }

    private void connectToDevice() {
        if (device == null) {
            device = rxBleClient.getBleDevice(HRM_MAC);
        }
    }

    private void onNotificationSetupFailure(Throwable throwable) {
        binding.deviceStatus.setText(R.string.connection_failed);
        Timber.e(throwable, "onNotificationSetupFailure() called");
    }

    private void onNotificationReceived(byte[] bytes) {
        if (bytes != null) {
            binding.pulsator.start();
            binding.heartIcon.setVisibility(View.VISIBLE);
            binding.deviceStatus.setText(R.string.connected);
            binding.bleRestart.setVisibility(View.GONE);
            byte pulse = bytes.length == 2 ? bytes[1] : bytes[0];
            binding.statusMessage.setText(String.valueOf(pulse));
            showWarning(pulse);
            Timber.d("pulse [length = %d] = %d", bytes.length, bytes.length == 2 ? bytes[1] : bytes[0]);
        } else {
            Timber.d("valueArray null");
        }
    }

    private void showWarning(byte pulse) {
        boolean lowPulse = pulse < 60;
        if (lowPulse && !binding.pulseWarningIcon.isShown()) {
            binding.pulseWarningIcon.setVisibility(View.VISIBLE);
            alarmSubscription = Observable.defer(() -> Observable.just(null).delay(2, TimeUnit.SECONDS))
                    .doOnCompleted(this::startAlarmService)
                    .subscribe();

        } else {
            binding.pulseWarningIcon.setVisibility(View.GONE);
            stopAlarmService();
        }
    }

    private void startAlarmService() {
        getActivity().startService(new Intent(getActivity(), HrmAlarmService.class));
    }

    private void stopAlarmService() {
        if (alarmSubscription != null && alarmSubscription.isUnsubscribed()) {
            alarmSubscription.unsubscribe();
        }
        getActivity().stopService(new Intent(getActivity(), HrmAlarmService.class));
    }


    private void notificationHasBeenSetUp() {
        Timber.d("notificationHasBeenSetUp: []");
    }


    private boolean isScanning() {
        return scanSubscription != null;
    }


    private void clearSubscription() {
        scanSubscription = null;
    }


  /*

    private void handleBleScanException(BleScanException bleScanException) {
        Timber.e(bleScanException, "handleBleScanException() called");
        switch (bleScanException.getReason()) {
            case BleScanException.BLUETOOTH_NOT_AVAILABLE:
                binding.statusMessage.setText(R.string.bluetooth_not_avaible);
                break;
            case BleScanException.BLUETOOTH_DISABLED:
                binding.statusMessage.setText(R.string.enable_bluetooth);
                break;
            case BleScanException.LOCATION_PERMISSION_MISSING:
                binding.statusMessage.setText(
                        "On Android 6.0 location permission is required. Implement Runtime Permissions");
                break;
            case BleScanException.LOCATION_SERVICES_DISABLED:
                binding.statusMessage.setText(R.string.disabled_location_premissions);
                break;
            case BleScanException.BLUETOOTH_CANNOT_START:
            default:
                binding.statusMessage.setText(R.string.unable_to_scan);
                break;
        }
    }

    private void onScanFailure(Throwable throwable) {
        if (throwable instanceof BleScanException) {
            handleBleScanException((BleScanException) throwable);
        } else {
            Timber.e(throwable, "onScanFailure()");
        }
    }


   private void onGetCharacteristicError(Throwable throwable) {
        Timber.e(throwable, "onGetCharacteristicError() called");
    }

    private void onCharacteristicReceived(BluetoothGattCharacteristic bluetoothGattCharacteristic) {
        byte[] valueArray = bluetoothGattCharacteristic.getValue();
        if (valueArray != null) {
            Timber.d("valueArray = %s", new String(valueArray));
        } else {
            Timber.d("valueArray null");
        }
    }

    private void onServicesDiscovered(RxBleDeviceServices rxBleDeviceServices) {
        for (BluetoothGattService service : rxBleDeviceServices.getBluetoothGattServices()) {
            Timber.d("onServicesDiscovered: service: [%s]", service.getUuid());
            for (BluetoothGattCharacteristic characteristic : service.getCharacteristics()) {
                Timber.d("onServicesDiscovered: characteristic: [%s]", characteristic.getUuid());
            }
        }
    }

    private void onServicesDiscoveryError(Throwable throwable) {
        Timber.e("onServicesDiscoveryError() called  with: throwable = [%s]", throwable);
    }

    private void onScanSuccess(RxBleScanResult rxBleScanResult) {
        Timber.d("onScanSuccess() called  with: rxBleScanResult [name : %s, mac: %s]", rxBleScanResult.getBleDevice().getName(), rxBleScanResult.getBleDevice().getMacAddress());
    }
*/
}
